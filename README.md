## AvatarImageView

## Introduction
When working on a phone book or other applications, the strategy for displaying a contact’s avatar is generally as follows: first determine whether there is a avatar picture, if there is, then display the picture directly; if not, display the first word of the contact’s name. Use this text as an avatar and add a background color.
This library offers a simple way to create an AvatarImageView on openharmony platform which can display circle text or circle image and SquareAvatarImageView which can be use to display round image.

### Features
- Show circular images.
- Show circular text.
- Show round images.
- The ratio of text size to circle radius can be adjusted.
- The display of pictures and texts is always centered; the centering rule for pictures is: if the picture is wider than the height, then the middle part of the square with height is intercepted. If the height of the picture is greater than the width, then the middle part of the width as a square is cut.
- Multiple text display with clip effect.
- You can customize the clip magnification ratio, use mask ratio to adjust the text magnification ratio.

## Usage Instructions

(1) Adding component to the layout file.

```XML
<AvatarImageView
                    ohos:id="$+id:aiv_left"
                    ohos:width="100vp"
                    ohos:height="100vp"
                    ohos:aiv_BoarderWidth="2"
                    ohos:aiv_ShowBoarder="true"
					ohos:aiv_TextSizeRatio="0.4f"/>
```
(2) How to set image or text

```XML
AvatarImageView aiv = (AvatarImageView) this.findViewById(R.id.aiv);
AvatarImageView aiv = (AvatarImageView) this.findComponentById(ResourceTable.Id_aiv_left);
aiv.setTextAndColor("openharmony", new Color(0x66660000));
aiv.setTextAndColorSeed("openharmony","openharmony platform");
aiv.setImageResource(ResourceTable.id_014);
aiv.setDrawable(drawable);
aiv.setBitmap(bitmap);
aiv.setImageDrawable(drawable);
```

## Installation Instructions

```
Method 1: Generate har package from library and add it to lib folder.
       add following code to gradle of entry
       implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
Method 2:
    allprojects{
        repositories{
            mavenCentral()
        }
    }
    implementation 'io.openharmony.tpc.thirdlib:AvatarImageView:1.0.1'

```


## License
-------
```

Copyright 2016 Carbs.Wang (AvatarImageView)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.