/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.carbs.harmony.avatarimageview.example.slice;

import cn.carbs.harmony.avatarimageview.example.ResourceTable;
import cn.carbs.harmony.avatarimageview.library.AvatarImageView;
import cn.carbs.harmony.avatarimageview.library.SquareAvatarImageView;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.utils.Color;

import static cn.carbs.harmony.avatarimageview.example.utils.Constants.longText;
import static cn.carbs.harmony.avatarimageview.example.utils.Constants.shortText;
import static cn.carbs.harmony.avatarimageview.example.utils.Constants.texts;

public class MainAbilitySlice extends AbilitySlice {


    private AvatarImageView aivLeft,aivRight;
    private SquareAvatarImageView sviCenter;
    private Button button0;
    private Button button1;
    private Button button2;
    private int mIndex;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        ComponentContainer rootLayout = (ComponentContainer) LayoutScatter.getInstance(this).
                parse(ResourceTable.Layout_ability_main, null, false);
        aivLeft = (AvatarImageView) rootLayout.findComponentById(ResourceTable.Id_aiv_left);
        aivLeft.setTextAndColor(shortText, new Color(0x66660000));

        sviCenter = (SquareAvatarImageView) rootLayout.findComponentById(ResourceTable.Id_svi_center);
        sviCenter.setImageResource(ResourceTable.Media_LightHouse);
        button0 = (Button) rootLayout.findComponentById(ResourceTable.Id_button0);
        button1 = (Button) rootLayout.findComponentById(ResourceTable.Id_button1);
        button1.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new LocalListViewAbilitySlice(), new Intent().addFlags(Intent.FLAG_ABILITY_NEW_MISSION));
            }
        });
        button0.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new NetListViewAbilitySlice(), new Intent().addFlags(Intent.FLAG_ABILITY_NEW_MISSION));
            }
        });
        aivRight = (AvatarImageView) rootLayout.findComponentById(ResourceTable.Id_aiv_right);
        aivRight.setTextAndColor(longText, new Color(0x66660000));
        button2 = (Button) rootLayout.findComponentById(ResourceTable.Id_button2);
        button2.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                aivLeft.setImageResource(ResourceTable.Media_LightHouse);
                int mod = mIndex % 3;
                if (mod == 0) {
                    aivLeft.setImageResource(ResourceTable.Media_LightHouse);
                    aivRight.setImageResource(ResourceTable.Media_LightHouse);
                } else if(mod == 1){
                    if(mIndex + 1 > texts.length()){
                        mIndex = 0;
                    }
                    String show = texts.substring(mIndex, mIndex + 1);
                    aivLeft.setTextAndColor(show, new Color(AvatarImageView.COLORS[4]));
                    aivRight.setTextAndColor(show, new Color(AvatarImageView.COLORS[4]));
                } else if(mod == 2){
                    if(mIndex + 2 > texts.length()){
                        mIndex = 0;
                    }
                    String show = texts.substring(mIndex, mIndex + 2);
                    aivLeft.setTextAndColor(show, new Color(AvatarImageView.COLORS[3]));
                    aivRight.setTextAndColor(show, new Color(AvatarImageView.COLORS[3]));
                }
                mIndex++;
            }
        });

        setUIContent(rootLayout);
    }
}
