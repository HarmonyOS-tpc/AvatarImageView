/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.carbs.harmony.avatarimageview.example.model;

public class Hero {

    public String nickName;
    public String realName;
    public String file;
    public Hero(String nickName, String realName, String file){
        this.nickName = nickName;
        this.realName = realName;
        this.file = file;
    }
    @Override
    public String toString() {
        return nickName + "\t" + realName;
    }
}
