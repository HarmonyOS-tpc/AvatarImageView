/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.carbs.harmony.avatarimageview.example.utils;

public class Constants {
    public static final String texts =   "朝辞白帝彩云间" +
            "千里江陵一日还" +
            "两岸猿声啼不住" +
            "轻舟已过万重山";

    public static final String shortText = "朝";
    public static final String longText = "安卓";
    public static final String buttonLocalRes = "To ListView Ability Loading LocalRes";
    public static final String buttonNetRes = "To ListView Ability Loading NetRes";
    public static final String id_prefix = "id_";



}
