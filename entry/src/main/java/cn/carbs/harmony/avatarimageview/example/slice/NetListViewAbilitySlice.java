/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.carbs.harmony.avatarimageview.example.slice;

import cn.carbs.harmony.avatarimageview.example.ResourceTable;
import cn.carbs.harmony.avatarimageview.example.adapter.ModelBaseAdapter;
import cn.carbs.harmony.avatarimageview.example.model.Model;
import cn.carbs.harmony.avatarimageview.library.Utils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;

import java.util.ArrayList;
import java.util.List;

import static cn.carbs.harmony.avatarimageview.example.utils.Constants.buttonNetRes;

public class NetListViewAbilitySlice extends AbilitySlice {

    ListContainer list;
    Button button;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        ComponentContainer rootLayout = (ComponentContainer)
                LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_view_list, null, false);
        setUIContent(rootLayout);
        list = (ListContainer) rootLayout.findComponentById(ResourceTable.Id_list);
        button = (Button) rootLayout.findComponentById(ResourceTable.Id_button);
        button.setText(buttonNetRes);
        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                ModelBaseAdapter adapter = new ModelBaseAdapter(rootLayout.getContext(), inflateData());
                list.setItemProvider(adapter);
            }
        });

    }

    private List<Model> inflateData() {
        List<Model> mModelList = new ArrayList<Model>();
        String[] urls = Utils.getStringArray(this, ResourceTable.Strarray_urls);
        mModelList.add(new Model("百度新闻", urls[0]));
        mModelList.add(new Model("图片1", urls[1]));
        mModelList.add(new Model("图片8",	urls[2]));
        mModelList.add(new Model("图片9",	urls[3]));
        mModelList.add(new Model("图片10", urls[4]));
        mModelList.add(new Model("图片11", urls[5]));
        mModelList.add(new Model("图片12", urls[6]));
        mModelList.add(new Model("图片13", urls[7]));
        mModelList.add(new Model("图片14",	urls[8]));
        mModelList.add(new Model("图片15",	urls[9]));
        mModelList.add(new Model("图片16",	urls[10]));
        mModelList.add(new Model("图片17", urls[11]));
        mModelList.add(new Model("图片18",	urls[12]));
        mModelList.add(new Model("图片19",	urls[13]));
        mModelList.add(new Model("图片20",	urls[14]));
        mModelList.add(new Model("图片21",	urls[15]));
        return mModelList;
    }
}
