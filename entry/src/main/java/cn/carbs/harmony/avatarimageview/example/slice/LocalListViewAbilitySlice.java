/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.carbs.harmony.avatarimageview.example.slice;

import cn.carbs.harmony.avatarimageview.example.ResourceTable;
import cn.carbs.harmony.avatarimageview.example.adapter.HeroBaseAdapter;
import cn.carbs.harmony.avatarimageview.example.model.Hero;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableSource;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.functions.Supplier;
import io.reactivex.rxjava3.openharmony.schedulers.OpenHarmonySchedulers;
import io.reactivex.rxjava3.observers.DisposableObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Button;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.global.resource.Entry;
import ohos.global.resource.RawFileEntry;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import static cn.carbs.harmony.avatarimageview.example.utils.Constants.buttonLocalRes;
import static cn.carbs.harmony.avatarimageview.example.utils.Constants.id_prefix;


public class LocalListViewAbilitySlice extends AbilitySlice {

    ListContainer list;
    Button button;

    private final CompositeDisposable disposables = new CompositeDisposable();
    private String fileDir = null;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        ComponentContainer rootLayout = (ComponentContainer)
                LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_view_list, null, false);
        setUIContent(rootLayout);
        list = (ListContainer) rootLayout.findComponentById(ResourceTable.Id_list);
        button = (Button) rootLayout.findComponentById(ResourceTable.Id_button);
        button.setText(buttonLocalRes);
        fileDir = getFilesDir().toString() + File.separator;
        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                disposables.add(sampleObservable()
                        // Run on a background thread
                        .subscribeOn(Schedulers.io())
                        // Be notified on the main thread
                        .observeOn(OpenHarmonySchedulers.mainThread())
                        .subscribeWith(new DisposableObserver<ArrayList<Hero>>() {
                            @Override
                            public void onComplete() {
                            }

                            @Override
                            public void onError(Throwable throwable) {
                            }

                            @Override
                            public void onNext(ArrayList<Hero> heroes) {
                                HeroBaseAdapter adapter = new HeroBaseAdapter(rootLayout.getContext(),heroes);
                                list.setItemProvider(adapter);
                            }
                        }));
            }
        });
    }

    private  void copyAssets(String fileDir){
        RawFileEntry rawFileEntry = this.getResourceManager().getRawFileEntry("entry/resources/rawfile");
        try {
            copyDataToSD(fileDir, rawFileEntry);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void copyDataToSD(String fileDir, RawFileEntry rawFileEntry) throws IOException {
        Entry[] resources = rawFileEntry.getEntries();
        OutputStream output = null;
        InputStream input = null;
        File file = null;
        String assetfileName;
        byte[] buffer = new byte[1024];
        for(Entry resource : resources) {
            assetfileName = resource.getPath();
            if(!resource.getPath().startsWith(id_prefix)) continue;
            file = new File(fileDir + assetfileName);
            if(file.exists()) continue;
            output = new FileOutputStream(file);
            input = this.getResourceManager().getRawFileEntry("entry/resources/rawfile/" + assetfileName).openRawFile();
            int length = input.read(buffer);
            while (length > 0) {
                output.write(buffer, 0, length);
                length = input.read(buffer);
            }
            output.flush();
            input.close();
            output.close();
        }
    }

    private String getFile(String fileDir, String id_prefix, int id){
        return fileDir + id_prefix + String.format("%03d", id) + ".jpg";
    }

     Observable<ArrayList<Hero>> sampleObservable() {
        return Observable.defer(new Supplier<ObservableSource<? extends ArrayList<Hero>>>() {
            @Override
            public ObservableSource<? extends ArrayList<Hero>> get() throws Throwable {
                // Do some long running operation
                Thread.sleep(5000);
                copyAssets(fileDir);
                return Observable.just(inflateData());
            }
        });
    }

    private ArrayList<Hero> inflateData(){
        ArrayList<Hero> heroes = new ArrayList<Hero>();
        int id_int = 1;
        heroes.add(new Hero("呼保义",	"宋江", getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("玉麒麟",	"卢俊义", 	 getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("智多星",	"吴用", 		 getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("入云龙",	"公孙胜", 	 getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("大刀",		"关胜", 		 getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("豹子头",	"林冲", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("霹雳火",	"秦明", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("双鞭将",	"呼延灼", 	getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("小李广",	"花荣",  	getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("小旋风",	"柴进", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("扑天雕",	"李应", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("美髯公",	"朱仝", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("花和尚",	"鲁智深", 	getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("行者",		"武松", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("双抢将",	"董平", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("没羽箭",	"张清", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("青面兽",	"杨志", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("金抢手",	"徐宁", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("急先锋",	"索超", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("神行太保",	"戴宗", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("赤发鬼",	"刘唐", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("黑旋风",	"李逵", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("九纹龙",	"史进", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("没遮拦",	"穆弘", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("插翅虎",	"雷横", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("混江龙",	"李俊", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("立地太岁",	"阮小二",	getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("船火儿",	"张横", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("短命二郎",	"阮小五",	getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("浪里白条",	"张顺", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("活阎罗",	"阮小七", 	getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("病关索",	"扬雄", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("拼命三郎",	"石秀", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("两头蛇",	"解珍", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("双尾蝎",	"解宝", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("浪子",		"燕青", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("神机军师",	"朱武", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("镇三山",	"黄信", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("病尉迟",	"孙立", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("丑郡马",	"宣赞", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("井木犴",	"郝思文", getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("百胜将",	"韩滔", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("天目将",	"彭玘", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("圣水将",	"单廷圭", 	getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("神火将",	"魏定国", 	getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("圣手书生",	"萧让", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("铁面孔目",	"裴宣", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("摩云金翅",	"欧鹏", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("火眼狻猊",	"邓飞", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("锦毛虎",	"燕顺", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("锦豹子",	"杨林", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("轰天雷",	"凌振", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("神算子",	"蒋敬", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("小温侯",	"吕方", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("赛仁贵",	"郭盛", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("神医",		"安道全", 	getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("紫髯伯",	"皇甫端", 	getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("矮脚虎",	"王英", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("一丈青",	"扈三娘", 	getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero(" 丧门神",	"鲍旭", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("混世魔王",	"樊瑞", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("毛头星",	"孔明", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("独火星",	"孔亮", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("八臂哪吒",	"项充", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("飞天大圣",	"李衮", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("玉臂匠",	"金大坚", 	getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("铁笛仙",	"马麟", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("出洞蛟",	"童威", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("翻江蜃",	"童猛", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("玉幡竿",	"孟康", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("通臂猿",	"侯健", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("跳涧虎",	"陈达", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("白花蛇",	"杨春", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("白面郎君",	"郑天寿",	getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("九尾龟",	"陶宗旺", 	getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("铁扇子",	"宋清", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("铁叫子",	"乐和", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("花项虎",	"龚旺", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("中箭虎",	"丁得孙", 	getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("小遮拦",	"穆春", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("操刀鬼",	"曹正", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("云里金刚",	"宋万", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("摸着天",	"杜迁", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("病大虫",	"薛永", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("金眼彪",	"施恩", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("打虎将",	"李忠", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("小霸王",	"周通", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("金钱豹子",	"汤隆", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("鬼脸儿",	"杜兴", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("出林龙",	"邹渊", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("独角龙",	"邹润", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("旱地忽律",	"朱贵 ", 	getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("笑面虎",	"朱富", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("铁臂膊",	"蔡福", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("一枝花",	"蔡庆", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("催命判官",	"李立", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("青眼虎",	"李云", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("没面目",	"焦挺", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("石将军",	"石勇", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("小尉迟",	"孙新", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("母大虫",	"顾大嫂", 	getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("菜园子",	"张青", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("母夜叉",	"孙二娘", 	getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("活闪婆",	"王定六", 	getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("险道神",	"郁保四", 	getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("白日鼠",	"白胜", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("鼓上蚤",	"时迁", 		getFile(fileDir, id_prefix, id_int++)));
        heroes.add(new Hero("金毛犬",	"段景住", 	getFile(fileDir, id_prefix, id_int++)));
        return heroes;
    }
}
