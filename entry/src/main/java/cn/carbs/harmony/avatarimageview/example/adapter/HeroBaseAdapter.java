/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.carbs.harmony.avatarimageview.example.adapter;

import cn.carbs.harmony.avatarimageview.example.ResourceTable;
import cn.carbs.harmony.avatarimageview.library.AvatarImageView;
import cn.carbs.harmony.avatarimageview.library.LogUtil;
import cn.carbs.harmony.avatarimageview.example.model.Hero;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.app.Context;
import ohos.media.image.DataSourceUnavailableException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.util.List;

public class HeroBaseAdapter extends BaseItemProvider {

    private static final String TAG = HeroBaseAdapter.class.getSimpleName();
    private Context context;
    private List<Hero> heroes;

    public HeroBaseAdapter(Context context,List<Hero> heroes) {
        this.context = context;
        this.heroes = heroes;
    }

    @Override
    public int getCount() {
        return heroes.size();
    }

    @Override
    public Object getItem(int i)
    {
        return heroes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        Hero hero = heroes.get(i);
        Text itemName;
        AvatarImageView itemAvatar;
        Component container = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_item_hero, null, false);
        itemAvatar = (AvatarImageView) container.findComponentById(ResourceTable.Id_item_avatar);
        itemName = (Text) container.findComponentById(ResourceTable.Id_item_name);
        itemName.setText(hero.toString());
        refreshAvatarImageView(itemAvatar,hero);
        return container;
    }

    private void refreshAvatarImageView(AvatarImageView aiv, Hero hero){
        if(aiv == null){
            return;
        }
        updateAvatarView(aiv, hero);
    }

    public static void updateAvatarView(AvatarImageView aiv, Hero hero) {
        PixelMap bitmap = null;
        String filePath = hero.file;
        if (filePath != null || filePath.length() != 0) {
            ImageSource.SourceOptions sourceOpts = new ImageSource.SourceOptions();
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            ImageSource imageSource = null;
            try {
                imageSource = ImageSource.create(filePath,sourceOpts);
            } catch (DataSourceUnavailableException ex) {
                LogUtil.error(TAG, "updateAvatarView -> DataSourceUnavailableException");
            }
            if (imageSource != null) {
                bitmap = imageSource.createPixelmap(decodingOptions);
            }

            if (bitmap != null) {
                aiv.setBitmap(bitmap);
            } else {
                if (hero.realName == null || hero.realName.trim().length() == 0) {
                    aiv.setTextAndColorSeed(" ", " ");
                } else {
                    aiv.setTextAndColorSeed(hero.realName, hero.realName);
                }
            }
        }
    }
}
